# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def guessing_game
  random_number = rand(1..100)
  num_of_guesses = 0
  while true
    puts "guess a number"
    input = gets.chomp.to_i
    num_of_guesses += 1
    if input == random_number
      puts "#{input} took #{num_of_guesses} guesses"
      break
    elsif input > random_number
      puts "#{input} was too high"
    else
      puts "#{input} was too low"
    end
  end
end

def file_shuffler(file_name)
  base = File.basename(file_name, ".*")
  File.open("#{base}-shuffled.txt", "w") do |f|
    File.readlines(file_name).shuffle.each do |line|
      f.puts line.chomp
    end
  end
end

if __FILE__ == $PROGRAM_NAME
  if ARGV.length == 1
    file_shuffler(ARGV.shift)
  else
    puts "ENTER FILENAME TO SHUFFLE:"
    file_name = gets.chomp
    file_shuffler(file_name)
  end
end
